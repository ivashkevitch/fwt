package blowfishtools

import (
	"bytes"
	"errors"
	"fmt"
	"golang.org/x/crypto/blowfish"
	"strconv"
	"strings"
)

var BlowfishErr = errors.New("blowfish err")
var IncorrectSolutionErr = errors.New("incorrect solution")

func Validate(challengeKey string, challengeCount int, solution []int) error {
	if len(solution) != challengeCount*2 {
		return IncorrectSolutionErr
	}

	bf, err := blowfish.NewCipher([]byte(challengeKey))
	if err != nil {
		return fmt.Errorf("%s: %w", BlowfishErr, err)
	}

	encryptedX := make([]byte, blowfish.BlockSize)
	encryptedY := make([]byte, blowfish.BlockSize)
	used := map[string]bool{}

	for i := 0; i < len(solution); i += 2 {
		x := strconv.Itoa(solution[i])
		y := strconv.Itoa(solution[i+1])

		usedKey := x + "-" + y
		if _, ok := used[usedKey]; ok {
			return IncorrectSolutionErr
		}

		dataX := strings.Repeat("X", 8-len(x)) + x
		bf.Encrypt(encryptedX, []byte(dataX))

		dataY := strings.Repeat("Y", 8-len(y)) + y
		bf.Encrypt(encryptedY, []byte(dataY))

		if bytes.Compare(encryptedX[5:], encryptedY[5:]) != 0 {
			return IncorrectSolutionErr
		}
		used[usedKey] = true
	}

	return nil
}
