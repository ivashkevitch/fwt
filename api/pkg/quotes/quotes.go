package quotes

import "math/rand"

func GetRandomQuote() string {
	quotes := []string{
		"Guard well your thoughts when alone and your words when accompanied. ― Roy T. Bennett",
		"I like to listen. I have learned a great deal from listening carefully. Most people never listen. ― Ernest Hemingway",
		"I think, that if the world were a bit more like ComicCon, it would be a better place. ― Matt Smith",
		"Quit being so hard on yourself. We are what we are; we love what we love. We don't need to justify it to anyone... not even to ourselves. ― Scott Lynch, The Republic of Thieves",
	}
	return quotes[rand.Intn(len(quotes))]
}
