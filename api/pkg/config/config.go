package config

import (
	"api/pkg/rsatools"
	"crypto/rsa"
	"time"
)

func Init() *Config {
	privateKey, publicKey := rsatools.GenerateRsaKeyPair(2048)

	c := &Config{
		Addr: ":8080",

		ChallengeSize:    1048576,
		ChallengeCount:   4096,
		ChallengeTimeout: time.Second * 10,

		SignPrivateKey: privateKey,
		SignPublicKey:  publicKey,
	}

	return c
}

type Config struct {
	Addr string

	SignPrivateKey *rsa.PrivateKey
	SignPublicKey  *rsa.PublicKey

	ChallengeSize    int
	ChallengeCount   int
	ChallengeTimeout time.Duration
}
