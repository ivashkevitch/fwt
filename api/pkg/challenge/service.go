package challenge

import (
	"api/pkg/blowfishtools"
	"api/pkg/config"
	"api/pkg/rsatools"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"log"
	"time"
)

var UnknownErr = errors.New("unknown error")

type Service struct {
	config *config.Config
}

func New(c *config.Config) *Service {
	return &Service{config: c}
}

func (s Service) Generate(ip string) (*Challenge, string, error) {
	challenge := &Challenge{
		Key:      uuid.New().String(),
		Size:     s.config.ChallengeSize,
		Count:    s.config.ChallengeCount,
		IP:       ip,
		Deadline: time.Now().Add(s.config.ChallengeTimeout),
	}

	challengeJson, err := json.Marshal(challenge)
	if err != nil {
		return nil, "", fmt.Errorf("err at marshaling challenge: %w", err)
	}

	sign, err := rsatools.Sign(string(challengeJson), s.config.SignPrivateKey)
	if err != nil {
		return nil, "", fmt.Errorf("err at signing challenge: %w", err)
	}

	return challenge, sign, nil
}

func (s Service) Check(challenge *Challenge, sign string, solution []int) error {
	if challenge.Deadline.Before(time.Now()) {
		return fmt.Errorf("deadline expired")
	}

	challengeJson, err := json.Marshal(challenge)
	if err != nil {
		log.Printf("err at marshaling challenge: %v", err)
		return UnknownErr
	}

	err = rsatools.VerifySignature(string(challengeJson), sign, s.config.SignPublicKey)
	if err != nil {
		return fmt.Errorf("bad sign")
	}

	startedAt := time.Now()
	err = blowfishtools.Validate(challenge.Key, challenge.Count, solution)
	finishedAt := time.Now()
	timeElapsed := finishedAt.Sub(startedAt)
	log.Printf("time elapsed for solution validation: %s\n", timeElapsed)

	if err != nil {
		if errors.Is(err, blowfishtools.IncorrectSolutionErr) {
			return err
		} else {
			log.Printf("err at blowfish check: %v", err)
			return UnknownErr
		}
	}

	return nil
}
