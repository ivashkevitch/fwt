package challenge

import "time"

type Challenge struct {
	Key   string `json:"key"`
	Size  int    `json:"size"`
	Count int    `json:"count"`

	IP string `json:"ip"`

	Deadline time.Time `json:"deadline"`
}
