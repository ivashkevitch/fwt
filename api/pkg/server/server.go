package server

import (
	challengePkg "api/pkg/challenge"
	"api/pkg/config"
	"api/pkg/quotes"
	"api/pkg/server/requests"
	"api/pkg/server/responses"
	"errors"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"log"
	"net/http"
)

type Server struct {
	config *config.Config
	router *echo.Echo

	challengeService *challengePkg.Service
}

func New(conf *config.Config, challengeService *challengePkg.Service) *Server {
	router := echo.New()
	router.HideBanner = true
	router.Use(
		middleware.Recover(),
	)

	return &Server{
		config: conf,
		router: router,

		challengeService: challengeService,
	}
}

func (s *Server) Run() {
	s.router.GET("/challenge", s.challenge)
	s.router.POST("/solution", s.solution)
	s.router.Logger.Fatal(s.router.Start(s.config.Addr))
}

func (s *Server) challenge(ctx echo.Context) error {
	c, sign, err := s.challengeService.Generate(ctx.RealIP())
	if err != nil {
		log.Println(err)
		return ctx.JSON(http.StatusInternalServerError, nil)
	}

	return ctx.JSON(http.StatusOK, responses.Challenge{
		Key:      c.Key,
		Size:     c.Size,
		Count:    c.Count,
		Deadline: c.Deadline,
		Sign:     sign,
	})
}

func (s *Server) solution(c echo.Context) error {
	req := &requests.Solution{}
	if err := c.Bind(req); err != nil {
		return err
	}

	challenge := &challengePkg.Challenge{
		Key:      req.Key,
		Size:     req.Size,
		Count:    req.Count,
		IP:       c.RealIP(),
		Deadline: req.Deadline,
	}

	err := s.challengeService.Check(challenge, req.Sign, req.Solution)
	if err != nil {
		if errors.Is(err, challengePkg.UnknownErr) {
			return c.JSON(http.StatusInternalServerError, &responses.InternalError{})
		}
		return c.JSON(http.StatusBadRequest, &responses.BadRequest{
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, struct {
		Message string
	}{quotes.GetRandomQuote()})
}
