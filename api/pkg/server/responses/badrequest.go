package responses

type BadRequest struct {
	Message string `json:"message"`
}
