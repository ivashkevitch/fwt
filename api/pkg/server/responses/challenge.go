package responses

import (
	"fmt"
	"time"
)

type responseTime time.Time

func (t responseTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).UTC().Format(time.RFC3339))
	return []byte(stamp), nil
}

type Challenge struct {
	Key   string `json:"key"`
	Size  int    `json:"size"`
	Count int    `json:"count"`

	Deadline time.Time `json:"deadline"`

	Sign string `json:"sign"`
}
