package requests

import "time"

type Solution struct {
	Key   string `json:"key"`
	Size  int    `json:"size"`
	Count int    `json:"count"`

	Deadline time.Time `json:"deadline"`
	Sign     string    `json:"sign"`

	Solution []int `json:"solution"`
}
