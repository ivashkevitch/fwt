package main

import (
	"api/pkg/challenge"
	"api/pkg/config"
	"api/pkg/server"
)

func main() {
	cfg := config.Init()

	challengeService := challenge.New(cfg)

	api := server.New(cfg, challengeService)
	api.Run()
}
