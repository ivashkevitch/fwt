package blowfishtools

import (
	"errors"
	"fmt"
	"golang.org/x/crypto/blowfish"
	"strconv"
	"strings"
)

var BlowfishErr = errors.New("blowfish err")
var NotSolvedErr = errors.New("not solved")

func Solve(challengeKey string, challengeCount int, challengeSize int) ([]int, error) {
	bf, err := blowfish.NewCipher([]byte(challengeKey))
	if err != nil {
		return nil, fmt.Errorf("%s: %w", BlowfishErr, err)
	}

	cache := make(map[string]int)
	dst := make([]byte, blowfish.BlockSize)

	for i := 0; i < challengeSize; i++ {
		number := strconv.Itoa(i)
		value := strings.Repeat("X", 8-len(number)) + number
		bf.Encrypt(dst, []byte(value))
		s := string(dst[5:])
		cache[s] = i
	}

	solution := make([]int, 0)
	for i := 0; i < challengeSize; i++ {
		number := strconv.Itoa(i)
		value := strings.Repeat("Y", 8-len(number)) + number
		bf.Encrypt(dst, []byte(value))
		s := string(dst[5:])
		if index, ok := cache[s]; ok {
			solution = append(solution, index, i)
		}
		if len(solution) == challengeCount*2 {
			return solution, nil
		}
	}
	return nil, NotSolvedErr
}
