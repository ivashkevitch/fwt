package main

import (
	"bytes"
	"client/pkg/api/requests"
	"client/pkg/api/responses"
	"client/pkg/blowfishtools"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

func main() {
	threads, err := strconv.Atoi(os.Getenv("THREADS"))
	if err != nil {
		log.Fatal(err)
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	for i := 0; i < threads; i++ {
		go func() {
			for {
				err := do()
				if err != nil {
					log.Printf("error: %s; wait 5 seconds\n", err)
					time.Sleep(time.Second * 5)
				}
			}
		}()
	}
	wg.Wait()
}

func do() error {
	challenge, err := getChallenge()
	if err != nil {
		return fmt.Errorf("error at getting challenge: %w", err)
	}

	startedAt := time.Now()
	solutionSequence, err := blowfishtools.Solve(challenge.Key, challenge.Count, challenge.Size)
	if err != nil {
		return fmt.Errorf("error at solving: %w\n", err)
	}
	finishAt := time.Now()
	elapsed := finishAt.Sub(startedAt)
	log.Printf("time elapsed for solution solving: %s\n", elapsed)

	solutionResponse, err := sendSolution(challenge, solutionSequence)
	if err != nil {
		return fmt.Errorf("error at sending solution: %w", err)
	}

	log.Printf("!!! solution accepted. answer: %s\n", solutionResponse.Message)
	return nil
}

func getChallenge() (*responses.Challenge, error) {
	getChallengeUrl := os.Getenv("CHALLENGE_URL")
	resp, err := http.Get(getChallengeUrl)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	challenge := &responses.Challenge{}
	jsonErr := json.Unmarshal(body, challenge)
	if jsonErr != nil {
		return nil, err
	}

	return challenge, err
}

func sendSolution(challenge *responses.Challenge, solutionSequence []int) (*responses.Solution, error) {
	solution := requests.Solution{
		Key:      challenge.Key,
		Size:     challenge.Size,
		Count:    challenge.Count,
		Deadline: challenge.Deadline,
		Sign:     challenge.Sign,

		Solution: solutionSequence,
	}

	solutionJson, err := json.Marshal(solution)
	if err != nil {
		return nil, err
	}

	postSolutionUrl := os.Getenv("SOLUTION_URL")
	resp, err := http.Post(postSolutionUrl, "application/json", bytes.NewBuffer(solutionJson))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	solutionResponse := &responses.Solution{}
	err = json.Unmarshal(body, solutionResponse)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		err = errors.New(resp.Status + " - " + string(body))
	}

	return solutionResponse, err
}
